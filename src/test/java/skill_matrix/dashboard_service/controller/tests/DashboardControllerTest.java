package skill_matrix.dashboard_service.controller.tests;

import static org.hamcrest.CoreMatchers.*;


import java.util.ArrayList;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

import skill_matrix.dashboard_service.constants.ProfileUpdateStatus;
import skill_matrix.dashboard_service.models.DashboardData;
import skill_matrix.dashboard_service.models.EmployeeDto;
import skill_matrix.dashboard_service.models.Skill;
import skill_matrix.dashboard_service.services.DashboardService;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
@AutoConfigureMockMvc
public class DashboardControllerTest {
	@Autowired
	private MockMvc mockMvc;
	@MockBean
	private DashboardService dashboardService;

	@Test
	public void shouldReturnDashboardDataObject() throws Exception {
		String dept = "Solutions";

		ArrayList<Skill> skills = new ArrayList<Skill>();
		Skill skill = new Skill(1, "Java");
		Skill skill2 = new Skill(2, "C#");
		Skill skill3 = new Skill(3, "C");
		skills.add(skill);
		skills.add(skill2);
		skills.add(skill3);

		ArrayList<EmployeeDto> employees = new ArrayList<EmployeeDto>();
		employees.add(new EmployeeDto(1, "Jathusan", "Kanthasamy", 2212, ProfileUpdateStatus.COMPLETE));
		employees.add(new EmployeeDto(2, "Chamalie", "Jathunarachi", 4433, ProfileUpdateStatus.DRAFT));
		employees.add(new EmployeeDto(3, "Ruchira", "Vitharma", 1542, ProfileUpdateStatus.NEW));
		employees.add(new EmployeeDto(4, "Vithan", "Thinav", 4244, ProfileUpdateStatus.DRAFT));
		employees.add(new EmployeeDto(5, "Chamila", "Rajan", 7443, ProfileUpdateStatus.DRAFT));
		employees.add(new EmployeeDto(6, "Patha", "Bina", 9244, ProfileUpdateStatus.NEW));
		for (EmployeeDto employee : employees) {
			employee.setDepartment(dept);
		}

		DashboardData dashData = new DashboardData(employees, 100, 70);
		ObjectMapper mapper = new ObjectMapper();

		Mockito.when(dashboardService.getDashboardData("Solutions")).thenReturn(dashData);

		this.mockMvc
				.perform(MockMvcRequestBuilders.get("/dashboard/{deptName}", "Solutions").contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.totalEmployees").value(100));
	}
	@Test
	public void shouldReturnDepartmentsTwo() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		ArrayList<String> departments = new ArrayList<String>();
		departments.add("Techlabs");
		departments.add("Marketing");

		Mockito.when(dashboardService.getAllDepartments()).thenReturn(departments);
		this.mockMvc
		.perform(MockMvcRequestBuilders.get("/dashboard/departments").contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.jsonPath("$", hasItem("Techlabs")));
	}
	
	@Test
	public void shouldReturnDepartments() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		ArrayList<String> departments = new ArrayList<String>();
		departments.add("Solutions");
		departments.add("Sales");
		departments.add("Operations");
		Mockito.when(dashboardService.getAllDepartments()).thenReturn(departments);
		this.mockMvc
		.perform(MockMvcRequestBuilders.get("/dashboard/departments").contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.jsonPath("$", hasItem("Solutions")));
	}

}
