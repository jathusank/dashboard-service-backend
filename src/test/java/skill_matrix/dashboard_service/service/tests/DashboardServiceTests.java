//package skill_matrix.dashboard_service.service.tests;
//
//import java.util.ArrayList;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//import org.junit.Before;
//import org.junit.jupiter.api.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mockito;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.context.TestConfiguration;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.context.annotation.Bean;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import org.junit.Assert;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//
//import org.mockito.junit.MockitoJUnitRunner;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.client.RestTemplate;
//
//import skill_matrix.dashboard_service.services.DashboardService;
//import skill_matrix.dashboard_service.services.DashboardServiceImpl;
//
//@RunWith(MockitoJUnitRunner.class)
//public class DashboardServiceTests {
//	@Value("${emp-profile.url}")
//	private String empProfBaseUrl;
//
//	@MockBean
//	private RestTemplate restTemplate;
//
//	@InjectMocks
//	private DashboardService dashService = new DashboardServiceImpl();
//
//	@Test
//	public void givenMockingIsDoneByMockito_whenGetIsCalled_shouldReturnMockedObject() {
//		ArrayList<String> departments = new ArrayList<String>();
//		departments.add("Techlabs");
//		departments.add("Marketing");
//		String[] depts = new String[] { "Techlabs", "Marketing" };
//		Mockito.when(restTemplate.getForEntity(empProfBaseUrl + "departments", String[].class))
//				.thenReturn(new ResponseEntity(depts, HttpStatus.OK));
//
//		ArrayList<String> departmentsReturn = dashService.getAllDepartments();
//		Assert.assertEquals(departmentsReturn, departments);
//	}
//}
