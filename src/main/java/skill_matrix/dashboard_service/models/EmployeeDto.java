package skill_matrix.dashboard_service.models;

import skill_matrix.dashboard_service.constants.ProfileUpdateStatus;

public class EmployeeDto {
	private long id;
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	private String firstName;

	private String lastName;

	private String department;

	private long employeeId;

	private ProfileUpdateStatus updateStatus;

	public EmployeeDto(long id, String firstName, String lastName, long employeeId, ProfileUpdateStatus updateStatus) {

		this.firstName = firstName;
		this.lastName = lastName;
		this.employeeId = employeeId;
		this.updateStatus = updateStatus;
	}

	public EmployeeDto() {
	}

	public ProfileUpdateStatus getUpdateStatus() {
		return updateStatus;
	}

	public void setUpdateStatus(ProfileUpdateStatus updateStatus) {
		this.updateStatus = updateStatus;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

}
