package skill_matrix.dashboard_service.models;

import java.util.ArrayList;
import java.util.List;

public class Department {

	private long id;
	private String name;

	private List<Employee> employees = new ArrayList<Employee>();

	private List<Skill> skills = new ArrayList<Skill>();

	public long getId() {
		return id;
	}

	public Department(long id, String name) {
		this.id = id;
		this.name = name;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public List<Skill> getSkills() {
		return skills;
	}

	public void setSkills(List<Skill> skills) {
		this.skills = skills;
	}

}
