package skill_matrix.dashboard_service.models;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

import skill_matrix.dashboard_service.constants.ProfileUpdateStatus;

public class Employee {

	private long id;
	@JsonProperty("fname")
	private String firstName;
	@JsonProperty("lname")
	private String lastName;

	private String department;

	private long employeeId;
	@JsonProperty("status")
	private ProfileUpdateStatus updateStatus;
	private ArrayList<Skill> skills = new ArrayList();

	public Employee(long id, String firstName, String lastName, long employeeId, ProfileUpdateStatus updateStatus) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.employeeId = employeeId;
		this.updateStatus = updateStatus;
	}

	public Employee() {
	}

	public long getId() {
		return id;
	}

	public ProfileUpdateStatus getUpdateStatus() {
		return updateStatus;
	}

	public void setUpdateStatus(ProfileUpdateStatus updateStatus) {
		this.updateStatus = updateStatus;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public ArrayList<Skill> getSkills() {
		return skills;
	}

	public void setSkills(ArrayList<Skill> skills) {
		this.skills = skills;
	}

}
