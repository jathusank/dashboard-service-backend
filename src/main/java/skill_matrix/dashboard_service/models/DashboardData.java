package skill_matrix.dashboard_service.models;

import java.util.ArrayList;

public class DashboardData {
	private ArrayList<EmployeeDto> employees;
	private int totalEmployees;
	private int profUpdatedEmployees;
	
	public DashboardData(ArrayList<EmployeeDto> employees, int totalEmployees, int profUpdatedEmployees) {
		super();
		this.employees = employees;
		this.totalEmployees = totalEmployees;
		this.profUpdatedEmployees = profUpdatedEmployees;
	}
	public DashboardData() {
		this.totalEmployees=0;
		this.profUpdatedEmployees= 0;
		this.employees = new ArrayList<EmployeeDto>();
	}

	public ArrayList<EmployeeDto> getEmployees() {
		return employees;
	}
	public void setEmployees(ArrayList<EmployeeDto> employees) {
		this.employees = employees;
	}
	public int getTotalEmployees() {
		return totalEmployees;
	}
	public void setTotalEmployees(int totalEmployees) {
		this.totalEmployees = totalEmployees;
	}
	public int getProfUpdatedEmployees() {
		return profUpdatedEmployees;
	}
	public void setProfUpdatedEmployees(int profUpdatedEmployees) {
		this.profUpdatedEmployees = profUpdatedEmployees;
	}
	
}
