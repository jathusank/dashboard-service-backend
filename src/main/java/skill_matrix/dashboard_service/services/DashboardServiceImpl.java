package skill_matrix.dashboard_service.services;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import skill_matrix.dashboard_service.constants.ProfileUpdateStatus;
import skill_matrix.dashboard_service.models.DashboardData;
import skill_matrix.dashboard_service.models.Employee;
import skill_matrix.dashboard_service.models.EmployeeDto;

@Service
public class DashboardServiceImpl implements DashboardService {
	@Value("${emp-profile.url}")
	private String empProfBaseUrl;

    private RestTemplate restTemplate = new RestTemplate();
	@Override
	public DashboardData getDashboardData(String deptName) {

//		String dept = "Solutions";
//
//		ArrayList<Employee> employees = new ArrayList<Employee>();
//		employees.add(new Employee(1, "Jathusan", "Kanthasamy", 2212, ProfileUpdateStatus.completed));
//		employees.add(new Employee(2, "Chamalie", "Jathunarachi", 4433, ProfileUpdateStatus.completed));
//		employees.add(new Employee(3, "Ruchira", "Vitharma", 1542, ProfileUpdateStatus.notUpdated));
//		employees.add(new Employee(4, "Vithan", "Thumilan", 4244, ProfileUpdateStatus.updated));
//		employees.add(new Employee(5, "Supun", "Gmage", 7443, ProfileUpdateStatus.draft));
//		employees.add(new Employee(6, "Pathi", "Deva", 9244, ProfileUpdateStatus.updated));
//		for (Employee employee : employees) {
//			employee.setDepartment(dept);
//		}
//
//		DashboardData dashData = new DashboardData(employees, 100, 70);
//		
		DashboardData dashData =  new DashboardData();
		int totalEmployees = 0;
		int profUpdatedEmployees = 0;

		// URL Parameters
		Map<String, String> params = new HashMap<String, String>();
		params.put("depName", deptName);

		try {
	
			ResponseEntity<Employee[]> response = restTemplate.getForEntity(empProfBaseUrl + "department/{depName}",Employee[].class, params);
			System.out.println(response.getStatusCodeValue());
			Employee[] employees = response.getBody();

			totalEmployees = employees.length;
			int id = 1;
			for (Employee emp : employees) {		
				emp.setDepartment(deptName);
				emp.setId(id);
				if (!emp.getUpdateStatus().equals(ProfileUpdateStatus.NEW)) {
					profUpdatedEmployees++;
				}
				id++;
			}
			ArrayList<Employee> employeesList = new ArrayList<Employee>(Arrays.asList(employees));

			ModelMapper modelMapper = new ModelMapper();

			TypeMap<Employee, EmployeeDto> typeMap = modelMapper.createTypeMap(Employee.class, EmployeeDto.class);

			typeMap.addMappings(mapper -> {
				mapper.map(emp -> emp.getFirstName(), EmployeeDto::setFirstName);
				mapper.map(emp -> emp.getLastName(), EmployeeDto::setLastName);
				mapper.map(emp -> emp.getUpdateStatus(), EmployeeDto::setUpdateStatus);

			});

			// Convert List of Entity objects to a List of DTOs objects
			Type listType = new TypeToken<List<EmployeeDto>>() {
			}.getType();
			ArrayList<EmployeeDto> empDtos = new ModelMapper().map(employeesList, listType);
			dashData = new DashboardData(empDtos, totalEmployees, profUpdatedEmployees);
			}
		
		
		catch(Exception ex){
			System.out.println(ex.getMessage());
			return dashData;
		}
			return dashData;

	}

	@Override
	public ArrayList<String> getAllDepartments() {

//		ArrayList<String> departments = new ArrayList();
//		departments.add("Solutions");
//		departments.add("TechLabs");
//		departments.add("Sales");
//		departments.add("HR");
//		departments.add("Operations");
//		departments.add("Marketing");
		ArrayList<String> deptList = new ArrayList<String>();
		try {

		ResponseEntity<String[]> response = restTemplate.getForEntity(empProfBaseUrl + "departments", String[].class);
		String[] departments = response.getBody();
		deptList = new ArrayList<String>(Arrays.asList(departments));
		}
		catch(Exception ex) {
			System.out.println(ex.getMessage());
			return deptList;
		}
		return deptList;
	}


}
