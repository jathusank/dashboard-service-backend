package skill_matrix.dashboard_service.services;

import java.util.ArrayList;

import skill_matrix.dashboard_service.models.DashboardData;

public interface DashboardService {
	public DashboardData getDashboardData(String deptName);
	public ArrayList<String> getAllDepartments();

}
