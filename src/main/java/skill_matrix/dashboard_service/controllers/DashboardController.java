package skill_matrix.dashboard_service.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import skill_matrix.dashboard_service.constants.ProfileUpdateStatus;
import skill_matrix.dashboard_service.models.DashboardData;
import skill_matrix.dashboard_service.models.EmployeeDto;
import skill_matrix.dashboard_service.services.DashboardService;

@RestController

public class DashboardController {
	@Autowired
	DashboardService dashboardService;
	@Autowired
    private SimpMessagingTemplate template;

	private int count = 0;

	@CrossOrigin()
	@RequestMapping(value = "/dashboard/departments", method = RequestMethod.GET)
	public ResponseEntity<List<String>> getDepartments() {
		System.out.println(this.getClass().getSimpleName() + " - Get all departments service is invoked.");
		return new ResponseEntity<>(dashboardService.getAllDepartments(), HttpStatus.OK);
	}

	@CrossOrigin()
	@RequestMapping(value = "/dashboard/{deptName}", method = RequestMethod.GET)
	public ResponseEntity<DashboardData> getDashboardData(@PathVariable String deptName) {

		System.out.println(this.getClass().getSimpleName() + " - Get DashboardData  by id is invoked.");
		return new ResponseEntity<>(dashboardService.getDashboardData(deptName), HttpStatus.OK);

	}
	
	@CrossOrigin()
	@RequestMapping(value = "/dashboard/employee/update/{empId}/{deptName}/{updateStatus}", method = RequestMethod.GET)
	public void  employeeUpdate(@PathVariable long empId,@PathVariable String deptName,@PathVariable ProfileUpdateStatus updateStatus ) {

		System.out.println(this.getClass().getSimpleName() + " - Employee updated service invoked.");
		EmployeeDto emp = new EmployeeDto();
		emp.setEmployeeId(empId);
		emp.setUpdateStatus(updateStatus);
		emp.setDepartment(deptName);
		// Push notifications to front-end
		template.convertAndSend("/employee/update", emp);

	}

	
//	@GetMapping("/notify")
//	public String getNotification() {
//
//		// Increment Notification by one
//		EmployeeDto emp = new EmployeeDto(1, "Narmatha", "Sutharshan", 1123, ProfileUpdateStatus.COMPLETE);
//		emp.setDepartment("Solutions");
//		// Push notifications to front-end
//		template.convertAndSend("/employee/update", emp);
//
//		return "Notifications successfully sent to Angular !";
//	}

}
