package skill_matrix.dashboard_service.constants;

public enum ProfileUpdateStatus {
	NEW,
	DRAFT,
	REVIEW,
	COMPLETE,
	INACTIVE

}
